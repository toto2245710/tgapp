# TourGuideApp
***

OpenClassrooms project number 8

TourGuide is an application that is part of TripMaster applications. It provides its users the nearby tourist attractions or attractive prices on hotel stays or tickets discount to various places.

To meet the growth on the touristic user client base, the project has to be improved in order to optimize performance for high volume user demands.


## Built with
***

- Java 11
- Gradle 7.5.1
